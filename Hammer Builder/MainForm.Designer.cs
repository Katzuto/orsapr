﻿namespace HammerBuilder
{
	partial class MainForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.headPanel = new System.Windows.Forms.Panel();
			this.hammerBuilderLabel = new System.Windows.Forms.Label();
			this.stickLLabel = new System.Windows.Forms.Label();
			this.stickDLabel = new System.Windows.Forms.Label();
			this.headLLabel = new System.Windows.Forms.Label();
			this.headHLabel = new System.Windows.Forms.Label();
			this.headWLabel = new System.Windows.Forms.Label();
			this.dividingLineFirst = new System.Windows.Forms.Panel();
			this.dividingLineSecond = new System.Windows.Forms.Panel();
			this.builderHammerButton = new System.Windows.Forms.Button();
			this.stickLUnitsLabel = new System.Windows.Forms.Label();
			this.stickDUnitsLabel = new System.Windows.Forms.Label();
			this.headLUnitsLabel = new System.Windows.Forms.Label();
			this.headHUnitsLabel = new System.Windows.Forms.Label();
			this.headWUnitsLabel = new System.Windows.Forms.Label();
			this.strikerWUnitsLabel = new System.Windows.Forms.Label();
			this.strikerLUnitsLabel = new System.Windows.Forms.Label();
			this.strikerHLabel = new System.Windows.Forms.Label();
			this.strikerWLabel = new System.Windows.Forms.Label();
			this.stickDiameterEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.stickLengthEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.strikerWidthEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.strikerHeightEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.headWidthEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.headHeightEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.headLengthEdit = new HammerBuilder.MaskedTextBoxWithBorderColor();
			this.headShapeLabel = new System.Windows.Forms.Label();
			this.headShapeComboBox = new System.Windows.Forms.ComboBox();
			this.headPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// headPanel
			// 
			this.headPanel.BackColor = System.Drawing.SystemColors.HotTrack;
			this.headPanel.Controls.Add(this.hammerBuilderLabel);
			this.headPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.headPanel.Location = new System.Drawing.Point(0, 0);
			this.headPanel.Name = "headPanel";
			this.headPanel.Size = new System.Drawing.Size(259, 50);
			this.headPanel.TabIndex = 0;
			// 
			// hammerBuilderLabel
			// 
			this.hammerBuilderLabel.AutoSize = true;
			this.hammerBuilderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.hammerBuilderLabel.ForeColor = System.Drawing.SystemColors.Window;
			this.hammerBuilderLabel.Location = new System.Drawing.Point(28, 10);
			this.hammerBuilderLabel.Name = "hammerBuilderLabel";
			this.hammerBuilderLabel.Size = new System.Drawing.Size(202, 29);
			this.hammerBuilderLabel.TabIndex = 0;
			this.hammerBuilderLabel.Text = "Hammer Builder";
			// 
			// stickLLabel
			// 
			this.stickLLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickLLabel.AutoSize = true;
			this.stickLLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.stickLLabel.Location = new System.Drawing.Point(15, 305);
			this.stickLLabel.Name = "stickLLabel";
			this.stickLLabel.Size = new System.Drawing.Size(98, 20);
			this.stickLLabel.TabIndex = 1;
			this.stickLLabel.Text = "Stick Length";
			// 
			// stickDLabel
			// 
			this.stickDLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickDLabel.AutoSize = true;
			this.stickDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.stickDLabel.Location = new System.Drawing.Point(15, 340);
			this.stickDLabel.Name = "stickDLabel";
			this.stickDLabel.Size = new System.Drawing.Size(113, 20);
			this.stickDLabel.TabIndex = 2;
			this.stickDLabel.Text = "Stick Diameter";
			// 
			// headLLabel
			// 
			this.headLLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headLLabel.AutoSize = true;
			this.headLLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headLLabel.Location = new System.Drawing.Point(15, 100);
			this.headLLabel.Name = "headLLabel";
			this.headLLabel.Size = new System.Drawing.Size(102, 20);
			this.headLLabel.TabIndex = 3;
			this.headLLabel.Text = "Head Length";
			// 
			// headHLabel
			// 
			this.headHLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headHLabel.AutoSize = true;
			this.headHLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headHLabel.Location = new System.Drawing.Point(15, 135);
			this.headHLabel.Name = "headHLabel";
			this.headHLabel.Size = new System.Drawing.Size(99, 20);
			this.headHLabel.TabIndex = 4;
			this.headHLabel.Text = "Head Height";
			// 
			// headWLabel
			// 
			this.headWLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headWLabel.AutoSize = true;
			this.headWLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headWLabel.Location = new System.Drawing.Point(15, 170);
			this.headWLabel.Name = "headWLabel";
			this.headWLabel.Size = new System.Drawing.Size(93, 20);
			this.headWLabel.TabIndex = 5;
			this.headWLabel.Text = "Head Width";
			// 
			// dividingLineFirst
			// 
			this.dividingLineFirst.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.dividingLineFirst.BackColor = System.Drawing.SystemColors.HotTrack;
			this.dividingLineFirst.Location = new System.Drawing.Point(10, 205);
			this.dividingLineFirst.Name = "dividingLineFirst";
			this.dividingLineFirst.Size = new System.Drawing.Size(235, 1);
			this.dividingLineFirst.TabIndex = 8;
			// 
			// dividingLineSecond
			// 
			this.dividingLineSecond.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.dividingLineSecond.BackColor = System.Drawing.SystemColors.HotTrack;
			this.dividingLineSecond.Location = new System.Drawing.Point(10, 290);
			this.dividingLineSecond.Name = "dividingLineSecond";
			this.dividingLineSecond.Size = new System.Drawing.Size(235, 1);
			this.dividingLineSecond.TabIndex = 9;
			// 
			// builderHammerButton
			// 
			this.builderHammerButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.builderHammerButton.BackColor = System.Drawing.SystemColors.HotTrack;
			this.builderHammerButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.HotTrack;
			this.builderHammerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.builderHammerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.builderHammerButton.ForeColor = System.Drawing.SystemColors.Window;
			this.builderHammerButton.Location = new System.Drawing.Point(65, 375);
			this.builderHammerButton.Name = "builderHammerButton";
			this.builderHammerButton.Size = new System.Drawing.Size(130, 40);
			this.builderHammerButton.TabIndex = 10;
			this.builderHammerButton.Text = "Build Hammer";
			this.builderHammerButton.UseVisualStyleBackColor = false;
			this.builderHammerButton.Click += new System.EventHandler(this.BuilderHammerButton_Click);
			// 
			// stickLUnitsLabel
			// 
			this.stickLUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickLUnitsLabel.AutoSize = true;
			this.stickLUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.stickLUnitsLabel.Location = new System.Drawing.Point(208, 305);
			this.stickLUnitsLabel.Name = "stickLUnitsLabel";
			this.stickLUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.stickLUnitsLabel.TabIndex = 18;
			this.stickLUnitsLabel.Text = "mm";
			// 
			// stickDUnitsLabel
			// 
			this.stickDUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickDUnitsLabel.AutoSize = true;
			this.stickDUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.stickDUnitsLabel.Location = new System.Drawing.Point(208, 340);
			this.stickDUnitsLabel.Name = "stickDUnitsLabel";
			this.stickDUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.stickDUnitsLabel.TabIndex = 19;
			this.stickDUnitsLabel.Text = "mm";
			// 
			// headLUnitsLabel
			// 
			this.headLUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headLUnitsLabel.AutoSize = true;
			this.headLUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headLUnitsLabel.Location = new System.Drawing.Point(208, 100);
			this.headLUnitsLabel.Name = "headLUnitsLabel";
			this.headLUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.headLUnitsLabel.TabIndex = 20;
			this.headLUnitsLabel.Text = "mm";
			// 
			// headHUnitsLabel
			// 
			this.headHUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headHUnitsLabel.AutoSize = true;
			this.headHUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headHUnitsLabel.Location = new System.Drawing.Point(208, 135);
			this.headHUnitsLabel.Name = "headHUnitsLabel";
			this.headHUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.headHUnitsLabel.TabIndex = 21;
			this.headHUnitsLabel.Text = "mm";
			// 
			// headWUnitsLabel
			// 
			this.headWUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headWUnitsLabel.AutoSize = true;
			this.headWUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headWUnitsLabel.Location = new System.Drawing.Point(208, 170);
			this.headWUnitsLabel.Name = "headWUnitsLabel";
			this.headWUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.headWUnitsLabel.TabIndex = 22;
			this.headWUnitsLabel.Text = "mm";
			// 
			// strikerWUnitsLabel
			// 
			this.strikerWUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerWUnitsLabel.AutoSize = true;
			this.strikerWUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.strikerWUnitsLabel.Location = new System.Drawing.Point(208, 255);
			this.strikerWUnitsLabel.Name = "strikerWUnitsLabel";
			this.strikerWUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.strikerWUnitsLabel.TabIndex = 30;
			this.strikerWUnitsLabel.Text = "mm";
			// 
			// strikerLUnitsLabel
			// 
			this.strikerLUnitsLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerLUnitsLabel.AutoSize = true;
			this.strikerLUnitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.strikerLUnitsLabel.Location = new System.Drawing.Point(208, 220);
			this.strikerLUnitsLabel.Name = "strikerLUnitsLabel";
			this.strikerLUnitsLabel.Size = new System.Drawing.Size(35, 20);
			this.strikerLUnitsLabel.TabIndex = 29;
			this.strikerLUnitsLabel.Text = "mm";
			// 
			// strikerHLabel
			// 
			this.strikerHLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerHLabel.AutoSize = true;
			this.strikerHLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.strikerHLabel.Location = new System.Drawing.Point(15, 220);
			this.strikerHLabel.Name = "strikerHLabel";
			this.strikerHLabel.Size = new System.Drawing.Size(106, 20);
			this.strikerHLabel.TabIndex = 26;
			this.strikerHLabel.Text = "Striker Height";
			// 
			// strikerWLabel
			// 
			this.strikerWLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerWLabel.AutoSize = true;
			this.strikerWLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.strikerWLabel.Location = new System.Drawing.Point(15, 255);
			this.strikerWLabel.Name = "strikerWLabel";
			this.strikerWLabel.Size = new System.Drawing.Size(100, 20);
			this.strikerWLabel.TabIndex = 25;
			this.strikerWLabel.Text = "Striker Width";
			// 
			// stickDiameterEdit
			// 
			this.stickDiameterEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickDiameterEdit.BorderColor = System.Drawing.Color.Gray;
			this.stickDiameterEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.stickDiameterEdit.Location = new System.Drawing.Point(150, 340);
			this.stickDiameterEdit.Name = "stickDiameterEdit";
			this.stickDiameterEdit.Size = new System.Drawing.Size(50, 20);
			this.stickDiameterEdit.TabIndex = 37;
			this.stickDiameterEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.stickDiameterEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// stickLengthEdit
			// 
			this.stickLengthEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stickLengthEdit.BorderColor = System.Drawing.Color.Gray;
			this.stickLengthEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.stickLengthEdit.Location = new System.Drawing.Point(150, 305);
			this.stickLengthEdit.Name = "stickLengthEdit";
			this.stickLengthEdit.Size = new System.Drawing.Size(50, 20);
			this.stickLengthEdit.TabIndex = 36;
			this.stickLengthEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.stickLengthEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// strikerWidthEdit
			// 
			this.strikerWidthEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerWidthEdit.BorderColor = System.Drawing.Color.Gray;
			this.strikerWidthEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.strikerWidthEdit.Location = new System.Drawing.Point(150, 255);
			this.strikerWidthEdit.Name = "strikerWidthEdit";
			this.strikerWidthEdit.Size = new System.Drawing.Size(50, 20);
			this.strikerWidthEdit.TabIndex = 35;
			this.strikerWidthEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.strikerWidthEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// strikerHeightEdit
			// 
			this.strikerHeightEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.strikerHeightEdit.BorderColor = System.Drawing.Color.Gray;
			this.strikerHeightEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.strikerHeightEdit.Location = new System.Drawing.Point(150, 220);
			this.strikerHeightEdit.Name = "strikerHeightEdit";
			this.strikerHeightEdit.Size = new System.Drawing.Size(50, 20);
			this.strikerHeightEdit.TabIndex = 34;
			this.strikerHeightEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.strikerHeightEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// headWidthEdit
			// 
			this.headWidthEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headWidthEdit.BorderColor = System.Drawing.Color.Gray;
			this.headWidthEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.headWidthEdit.Location = new System.Drawing.Point(150, 170);
			this.headWidthEdit.Name = "headWidthEdit";
			this.headWidthEdit.Size = new System.Drawing.Size(50, 20);
			this.headWidthEdit.TabIndex = 33;
			this.headWidthEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.headWidthEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// headHeightEdit
			// 
			this.headHeightEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headHeightEdit.BorderColor = System.Drawing.Color.Gray;
			this.headHeightEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.headHeightEdit.Location = new System.Drawing.Point(150, 135);
			this.headHeightEdit.Name = "headHeightEdit";
			this.headHeightEdit.Size = new System.Drawing.Size(50, 20);
			this.headHeightEdit.TabIndex = 32;
			this.headHeightEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.headHeightEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// headLengthEdit
			// 
			this.headLengthEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headLengthEdit.BorderColor = System.Drawing.Color.Gray;
			this.headLengthEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.headLengthEdit.Location = new System.Drawing.Point(150, 100);
			this.headLengthEdit.Name = "headLengthEdit";
			this.headLengthEdit.Size = new System.Drawing.Size(50, 20);
			this.headLengthEdit.TabIndex = 31;
			this.headLengthEdit.ValidatingType = typeof(int);
			this.headLengthEdit.TextChanged += new System.EventHandler(this.MaskedTextBoxWithBorderColor_TextChanged);
			this.headLengthEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaskedTextBoxWithBorderColor_KeyPress);
			// 
			// headShapeLabel
			// 
			this.headShapeLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.headShapeLabel.AutoSize = true;
			this.headShapeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.headShapeLabel.Location = new System.Drawing.Point(15, 65);
			this.headShapeLabel.Name = "headShapeLabel";
			this.headShapeLabel.Size = new System.Drawing.Size(99, 20);
			this.headShapeLabel.TabIndex = 38;
			this.headShapeLabel.Text = "Head Shape";
			// 
			// headShapeComboBox
			// 
			this.headShapeComboBox.FormattingEnabled = true;
			this.headShapeComboBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.headShapeComboBox.Items.AddRange(new object[] {
            "4-angle prism",
            "Cylinder"});
			this.headShapeComboBox.Location = new System.Drawing.Point(150, 67);
			this.headShapeComboBox.Name = "headShapeComboBox";
			this.headShapeComboBox.Size = new System.Drawing.Size(93, 21);
			this.headShapeComboBox.TabIndex = 30;
			this.headShapeComboBox.SelectedIndexChanged += new System.EventHandler(this.headShapeComboBox_SelectedIndexChanged);
			this.headShapeComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HeadShapeComboBox_KeyPress);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(259, 427);
			this.Controls.Add(this.headShapeComboBox);
			this.Controls.Add(this.headShapeLabel);
			this.Controls.Add(this.stickDiameterEdit);
			this.Controls.Add(this.stickLengthEdit);
			this.Controls.Add(this.strikerWidthEdit);
			this.Controls.Add(this.strikerHeightEdit);
			this.Controls.Add(this.headWidthEdit);
			this.Controls.Add(this.headHeightEdit);
			this.Controls.Add(this.headLengthEdit);
			this.Controls.Add(this.strikerWUnitsLabel);
			this.Controls.Add(this.strikerLUnitsLabel);
			this.Controls.Add(this.strikerHLabel);
			this.Controls.Add(this.strikerWLabel);
			this.Controls.Add(this.headWUnitsLabel);
			this.Controls.Add(this.headHUnitsLabel);
			this.Controls.Add(this.headLUnitsLabel);
			this.Controls.Add(this.stickDUnitsLabel);
			this.Controls.Add(this.stickLUnitsLabel);
			this.Controls.Add(this.builderHammerButton);
			this.Controls.Add(this.dividingLineSecond);
			this.Controls.Add(this.dividingLineFirst);
			this.Controls.Add(this.headWLabel);
			this.Controls.Add(this.headHLabel);
			this.Controls.Add(this.headLLabel);
			this.Controls.Add(this.stickDLabel);
			this.Controls.Add(this.stickLLabel);
			this.Controls.Add(this.headPanel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Hammer Builder";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.headPanel.ResumeLayout(false);
			this.headPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel headPanel;
		private System.Windows.Forms.Label hammerBuilderLabel;
		private System.Windows.Forms.Label stickLLabel;
		private System.Windows.Forms.Label stickDLabel;
		private System.Windows.Forms.Label headLLabel;
		private System.Windows.Forms.Label headHLabel;
		private System.Windows.Forms.Label headWLabel;
		private System.Windows.Forms.Panel dividingLineFirst;
		private System.Windows.Forms.Panel dividingLineSecond;
		private System.Windows.Forms.Button builderHammerButton;
		private System.Windows.Forms.Label stickLUnitsLabel;
		private System.Windows.Forms.Label stickDUnitsLabel;
		private System.Windows.Forms.Label headLUnitsLabel;
		private System.Windows.Forms.Label headHUnitsLabel;
		private System.Windows.Forms.Label headWUnitsLabel;
		private System.Windows.Forms.Label strikerWUnitsLabel;
		private System.Windows.Forms.Label strikerLUnitsLabel;
		private System.Windows.Forms.Label strikerHLabel;
		private System.Windows.Forms.Label strikerWLabel;
		private MaskedTextBoxWithBorderColor headLengthEdit;
		private MaskedTextBoxWithBorderColor headHeightEdit;
		private MaskedTextBoxWithBorderColor headWidthEdit;
		private MaskedTextBoxWithBorderColor strikerHeightEdit;
		private MaskedTextBoxWithBorderColor strikerWidthEdit;
		private MaskedTextBoxWithBorderColor stickLengthEdit;
		private MaskedTextBoxWithBorderColor stickDiameterEdit;
		private System.Windows.Forms.Label headShapeLabel;
		private System.Windows.Forms.ComboBox headShapeComboBox;
	}
}

