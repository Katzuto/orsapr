﻿
namespace HammerBuilder
{
	/// <summary>
	/// Parameters stick hammer
	/// </summary>
	public class StickParameters
	{
		// ****************
		// Public
		#region Public Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="height"> Hammer stick length </param>
		/// <param name="width"> Hammer stick diameter </param>
		public StickParameters(double length, double diameter)
		{
			_length = length;
			_diameter = diameter;
		}

		#endregion


		// ****************
		// Properties
		#region Properties
		
		/// <summary>
		/// Hammer stick length
		/// </summary>
		public double Length { get =>_length; }

		/// <summary>
		/// Hammer stick diameter
		/// </summary>
		public double Diameter { get => _diameter; }

		#endregion


		// ****************
		// Private
		#region Private Field

		/// <summary>
		/// Hammer stick length
		/// </summary>
		private double _length;

		/// <summary>
		/// Hammer stick diameter
		/// </summary>
		private double _diameter;

		#endregion
	}
}
