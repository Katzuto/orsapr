﻿namespace HammerBuilder
{
	/// <summary>
	/// New data type to identify form hammer
	/// </summary>
	public enum FormHead
	{
		Prism = 0,
		Cylinder
	}
}
