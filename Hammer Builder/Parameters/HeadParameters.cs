﻿
namespace HammerBuilder
{
	/// <summary>
	/// Parameters head hammer
	/// </summary>
	public class HeadParameters
	{
		// ****************
		// Public
		#region Public Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="length"> Hammer head length </param>
		/// <param name="height"> Hammer head height </param>
		/// <param name="width"> Hammer head width </param>
		public HeadParameters(double length, double height, double width, FormHead form)
		{
			_length = length;
			_height = height;
			_width = width;
			_form = form;
		}

		#endregion


		// ****************
		// Properties
		#region Properties

		/// <summary>
		/// Hammer head length 
		/// </summary>
		public double Length { get => _length; }

		/// <summary>
		/// Hammer head height 
		/// </summary>
		public double Height { get => _height; }

		/// <summary>
		/// Hammer head width 
		/// </summary>
		public double Width { get => _width; }

		/// <summary>
		/// 
		/// </summary>
		public FormHead Form { get => _form; }

		#endregion


		// ****************
		// Private
		#region Private Field

		/// <summary>
		/// Hammer head length 
		/// </summary>
		private double _length;

		/// <summary>
		/// Hammer head height 
		/// </summary>
		private double _height;

		/// <summary>
		/// Hammer head width 
		/// </summary>
		private double _width;

		/// <summary>
		/// 
		/// </summary>
		private FormHead _form;

		#endregion
	}
}
