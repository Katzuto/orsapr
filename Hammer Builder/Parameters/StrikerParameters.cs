﻿
namespace HammerBuilder
{
	/// <summary>
	/// Parameters striker hammer
	/// </summary>
	public class StrikerParameters
	{
		// ****************
		// Public
		#region Public Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="height"> Hammer striker height </param>
		/// <param name="width"> Hammer striker width </param>
		public StrikerParameters(double height, double width)
		{
			_height = height;
			_width = width;
		}

		#endregion


		// ****************
		// Properties
		#region Properties

		/// <summary>
		/// Hammer striker height
		/// </summary>
		public double Height { get => _height; }

		/// <summary>
		/// Hammer striker width
		/// </summary>
		public double Width { get => _width; }

		#endregion


		// ****************
		// Private
		#region Private Field

		/// <summary>
		/// Hammer striker height
		/// </summary>
		private double _height;

		/// <summary>
		/// Hammer striker width
		/// </summary>
		private double _width;

		#endregion
	}
}
