﻿using System.Collections.Generic;

namespace HammerBuilder
{
	/// <summary>
	/// Hammer parameters storage class
	/// </summary>
	public class HammerParameters
	{
		// ****************
		// Public
		#region Public Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="hammerParameters"> List of all hammer parameters </param>
		public HammerParameters(List<double> hammerParameters)
		{
			HeadParameters headParameters = new HeadParameters(
				hammerParameters[(int)ParametersType.HeadLength],
				hammerParameters[(int)ParametersType.HeadHeight],
				hammerParameters[(int)ParametersType.HeadWidth],
				(FormHead)((int)hammerParameters
					[(int)ParametersType.HeadForm]));

			StrikerParameters strikerParameters = new StrikerParameters(
				hammerParameters[(int)ParametersType.StrikerHeight],
				hammerParameters[(int)ParametersType.StrikerWidth]);

			StickParameters stickParameters = new StickParameters(
				hammerParameters[(int)ParametersType.StickLength],
				hammerParameters[(int)ParametersType.StickDiameter]);

			if (Validate(headParameters, strikerParameters, stickParameters))
			{
				_headParameters = headParameters;
				_strikerParameters = strikerParameters;
				_stickParameters = stickParameters;
			}
		}


		/// <summary>
		/// Checking model parameters
		/// </summary>
		/// <param name="headParameters"> Hammer head parameters </param>
		/// <param name="strikerParameters"> Hammer striker parameters </param>
		/// <param name="stickParameters"> Hammer stick parameters </param>
		/// <returns> Check done or not </returns>
		private bool Validate(HeadParameters headParameters,
			StrikerParameters strikerParameters,
			StickParameters stickParameters)
		{
			const double minHeadLength = 100;
			const double maxHeadLength = 250;

			bool checkHeadLength = 
				(headParameters.Length >= minHeadLength)
				&& (headParameters.Length <= maxHeadLength);

			if (!checkHeadLength)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorHeadLength,
					"The head length are not correct.\n" +
					"Please observe the following relationship:\n" +
					"100 mm <= head length <= 250 mm");
				return false;
			}

			const double minHeadHeight = 50;
			const double maxHeadHeight = 100;

			bool checkHeadHeight = 
				(headParameters.Height >= minHeadHeight)
				&& (headParameters.Height <= maxHeadHeight);

			if (!checkHeadHeight)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorHeadHeight,
					"The head height are not correct.\n" +
					"Please observe the following relationship:\n" +
					"50 mm <= head height <= 100 mm");
				return false;
			}

			const double minHeadWidth = 50;
			const double maxHeadWidth = 100;

			bool checkHeadWidth = 
				(headParameters.Width >= minHeadWidth)
				&& (headParameters.Width <= maxHeadWidth);

			if (!checkHeadWidth)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorHeadWidth,
					"The head width are not correct.\n" +
					"Please observe the following relationship:\n" +
					"50 mm <= head width <= 100 mm");
				return false;
			}

			const double minStrikerHeight = 20;

			bool checkStrikerHeight = 
				(strikerParameters.Height < headParameters.Height)
				&& (strikerParameters.Height >= minStrikerHeight);

			if (!checkStrikerHeight)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorStrikerHeight,
					"The striker height are not correct.\n" +
					"Please observe the following relationship:\n" +
					"20 mm <= striker height < head height");
				return false;
			}

			const double minStrikerWidth = 20;

			bool checkStrikerWidth = 
				(strikerParameters.Width < headParameters.Width)
				&& (strikerParameters.Width >= minStrikerWidth);

			if (!checkStrikerWidth)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorStrikerWidth,
					"The striker width are not correct.\n" +
					"Please observe the following relationship:\n" +
					"20 mm <= striker width < head width");
				return false;
			}

			const double maxStickLength = 1000;

			bool checkStickLength =
				(stickParameters.Length > headParameters.Height)
				&& (stickParameters.Length <= maxStickLength);

			if (!checkStickLength)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorStickLength,
					"The stick length are not correct.\n" +
					"Please observe the following relationship:\n" +
					"head length < stick length <= 1000 mm");
				return false;
			}

			const double minStickDiameter = 10;

			bool checkStickDiameter =
				(stickParameters.Diameter >= minStickDiameter)
				&& (stickParameters.Diameter < headParameters.Width);

			if (!checkStickDiameter)
			{
				PluginReporter.Instance().Add(
					PluginReporter.TypeError.ErrorStickDiameter,
					"The stick diameter are not correct.\n" +
					"Please observe the following relationship:\n" +
					"10 mm <= stick length < head width");
				return false;
			}

			return true;
		}

		#endregion


		// ****************
		// Properties
		#region Properties

		/// <summary>
		/// Hammer head parameters
		/// </summary>
		public HeadParameters HeadParam { get => _headParameters; }

		/// <summary>
		/// Hammer striker parameters
		/// </summary>
		public StrikerParameters StrikerParam { get => _strikerParameters; }

		/// <summary>
		/// Hammer stick parameters
		/// </summary>
		public StickParameters StickParam { get => _stickParameters; }

		#endregion


		// ****************
		// Private
		#region Private Field

		/// <summary>
		/// Hammer head parameters
		/// </summary>
		private HeadParameters _headParameters;

		/// <summary>
		/// Hammer striker parameters
		/// </summary>
		private StrikerParameters _strikerParameters;

		/// <summary>
		/// Hammer stick parameters
		/// </summary>
		private StickParameters _stickParameters;

		#endregion
	}


	/// <summary>
	/// New data type to identify hammer parameters
	/// </summary>
	internal enum ParametersType
	{
		HeadLength = 0,
		HeadHeight,
		HeadWidth,
		HeadForm,
		StrikerHeight = 4,
		StrikerWidth,
		StickLength = 6,
		StickDiameter
	}
}
