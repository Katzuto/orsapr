﻿using Inventor;
using Application = Inventor.Application;


namespace HammerBuilder
{
	/// <summary>
	/// Hammer making class
	/// </summary>
	public class HammerManager
	{
		// ****************
		// Public
		#region Public Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="api"> Wrapper API Inventor </param>
		/// <param name="modelParameters"> Hammer parameters </param>
		public HammerManager(InventorAPI api, HammerParameters modelParameters)
		{
			_api = api;
			_modelParameters = modelParameters;
			_objCollection = _api.InventorApplication.TransientObjects.
				CreateObjectCollection();
		}


		/// <summary>
		/// Making an entire hammer model in Inventor
		/// </summary>
		public void CreateHammer()
		{
			if (_modelParameters != null)
			{
				CreateHead();
				CreateStriker();
				CreateStick();
			}
		}

		#endregion


		// ****************
		// Private
		#region Private Field

		/// <summary>
		/// Hammer parameters
		/// </summary>
		private HammerParameters _modelParameters;

		/// <summary>
		/// Wrapper API Inventor
		/// </summary>
		private InventorAPI _api;

		/// <summary>
		/// Collection of sketches for creating strikers
		/// </summary>
		private ObjectCollection _objCollection;

		#endregion


		#region Private Methods

		/// <summary>
		/// Making a head hammer in Inventor
		/// </summary>
		private void CreateHead()
		{
			switch (_modelParameters.HeadParam.Form)
			{
				case FormHead.Prism:
					// Building the hammer head 
					_api.MakeNewSketch(3, 0);
					_api.DrawRectangle(0, 0, _modelParameters.HeadParam.Height,
						_modelParameters.HeadParam.Width);
					_api.Extrude(_modelParameters.HeadParam.Length);
					break;
				case FormHead.Cylinder:
					// Building the hammer head 
					_api.MakeNewSketch(3, 0);
					_api.DrawCircle(_modelParameters.HeadParam.Height / 2,
						_modelParameters.HeadParam.Height / 2,
						_modelParameters.HeadParam.Height);
					_api.Extrude(_modelParameters.HeadParam.Length);
					break;
			}

			// Making a cut in the head under the stick
			_api.MakeNewSketch(2, _modelParameters.HeadParam.Height);
			_api.DrawCircle((_modelParameters.HeadParam.Width / -2),
				(_modelParameters.HeadParam.Length / 2), 
				_modelParameters.StickParam.Diameter);
			_api.CutOut(_modelParameters.HeadParam.Height,
				PartFeatureExtentDirectionEnum.kNegativeExtentDirection);
		}


		/// <summary>
		/// Making a striker hammer in Inventor
		/// </summary>
		private void CreateStriker()
		{
			switch (_modelParameters.HeadParam.Form)
			{
				case FormHead.Prism:
					CreateRectangleStriker();
					break;

				case FormHead.Cylinder:
					CreateCylindricalStriker();
					break;
			}

		}


		/// <summary>
		/// Making a stick hammer in Inventor
		/// </summary>
		private void CreateStick()
		{
			// The number 0.01 in order that the handle does not merge
			// with the head
			_api.MakeNewSketch(2, _modelParameters.HeadParam.Height + 0.01);
			_api.DrawCircle((_modelParameters.HeadParam.Width / -2),
				(_modelParameters.HeadParam.Length / 2),
				_modelParameters.StickParam.Diameter);
			_api.Extrude(_modelParameters.StickParam.Length,
				PartFeatureExtentDirectionEnum.kNegativeExtentDirection);
		}


		/// <summary>
		/// Algorithm for building a rectangular hammer striker
		/// </summary>
		private void CreateRectangleStriker()
		{
			_api.MakeNewSketch(3, 0);
			_api.DrawRectangle(0, 0, _modelParameters.HeadParam.Height,
				_modelParameters.HeadParam.Width);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			// Upper left corner of Impact Surface
			double impactSurfaceX1 = (_modelParameters.HeadParam.Height / 2) -
				(_modelParameters.StrikerParam.Height / 2);
			double impactSurfaceY1 = (_modelParameters.HeadParam.Width / 2) -
				(_modelParameters.StrikerParam.Width / 2);

			// Bottom right corner of Impact Surface
			double impactSurfaceX2 = (_modelParameters.HeadParam.Height / 2) +
				(_modelParameters.StrikerParam.Height / 2);
			double impactSurfaceY2 = (_modelParameters.HeadParam.Width / 2) +
				(_modelParameters.StrikerParam.Width / 2);

			const double distanceStrikerHead = 7.5;

			_api.MakeNewSketch(3, 0 - distanceStrikerHead);
			_api.DrawRectangle(impactSurfaceX1, impactSurfaceY1,
				impactSurfaceX2, impactSurfaceY2);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.Loft(_objCollection);

			_objCollection.Clear();

			_api.MakeNewSketch(3, _modelParameters.HeadParam.Length);
			_api.DrawRectangle(0, 0, _modelParameters.HeadParam.Height,
				_modelParameters.HeadParam.Width);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.MakeNewSketch(3, _modelParameters.HeadParam.Length +
				distanceStrikerHead);
			_api.DrawRectangle(impactSurfaceX1, impactSurfaceY1,
				impactSurfaceX2, impactSurfaceY2);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.Loft(_objCollection);
		}


		/// <summary>
		/// Algorithm for building a cylindrical hammer striker
		/// </summary>
		private void CreateCylindricalStriker()
		{
			const double distanceStrikerHead = 7.5;

			_api.MakeNewSketch(3, 0);
			_api.DrawCircle(_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.MakeNewSketch(3, 0 - distanceStrikerHead);
			_api.DrawCircle(_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height / 2,
				_modelParameters.StrikerParam.Height);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.Loft(_objCollection);

			_objCollection.Clear();

			_api.MakeNewSketch(3, _modelParameters.HeadParam.Length);
			_api.DrawCircle(_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.MakeNewSketch(3, _modelParameters.HeadParam.Length +
				distanceStrikerHead);
			_api.DrawCircle(_modelParameters.HeadParam.Height / 2,
				_modelParameters.HeadParam.Height / 2,
				_modelParameters.StrikerParam.Height);

			_objCollection.Add(_api.CurrentSketch.Profiles.AddForSolid());

			_api.Loft(_objCollection);
		}

		#endregion
	}
}
