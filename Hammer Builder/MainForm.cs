﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace HammerBuilder
{
	/// <summary>
	/// Main form class
	/// </summary>
	public partial class MainForm : Form
	{
		/// <summary>
		/// Hammer parameters
		/// </summary>
		private HammerParameters _hammerParameters;

		/// <summary>
		/// Hammer manager
		/// </summary>
		private HammerManager _hammerManager;

		/// <summary>
		/// Inventor API
		/// </summary>
		private InventorAPI _inventorAPI;

		/// <summary>
		/// 
		/// </summary>
		private Dictionary<PluginReporter.TypeError,
			MaskedTextBoxWithBorderColor> _typeErrorToMaskedTextBoxDictionary;


		/// <summary>
		/// Constructor class
		/// </summary>
		public MainForm()
		{
			InitializeComponent();

			PluginReporter.Instance().OnAdd += ShowError;

			headShapeComboBox.SelectedIndex = (int)FormHead.Prism;

			_typeErrorToMaskedTextBoxDictionary =
				new Dictionary<PluginReporter.TypeError,
				MaskedTextBoxWithBorderColor>()
				{
					{ PluginReporter.TypeError.ErrorHeadLength, headLengthEdit},
					{ PluginReporter.TypeError.ErrorHeadHeight, headHeightEdit},
					{ PluginReporter.TypeError.ErrorHeadWidth, headWidthEdit},
					{ PluginReporter.TypeError.ErrorStrikerHeight, strikerHeightEdit},
					{ PluginReporter.TypeError.ErrorStrikerWidth, strikerWidthEdit},
					{ PluginReporter.TypeError.ErrorStickLength, stickLengthEdit},
					{ PluginReporter.TypeError.ErrorStickDiameter, stickDiameterEdit},
				};
		}


		/// <summary>
		/// Error display
		/// </summary>
		private void ShowError()
		{
			_typeErrorToMaskedTextBoxDictionary[PluginReporter.Instance().
				LastAddedError].BorderColor = System.Drawing.Color.Red;
			
			MessageBox.Show(PluginReporter.Instance().GetLastError(),
				"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}


		private void MainForm_Load(object sender, EventArgs e)
		{
			foreach (var i in _typeErrorToMaskedTextBoxDictionary.Values)
			{
				i.BorderColor = System.Drawing.Color.Gray;
			}
		}


		private void BuilderHammerButton_Click(object sender, EventArgs e)
		{
			if (headShapeComboBox.SelectedIndex == 1)
			{
				headWidthEdit.Text = headHeightEdit.Text;
				strikerWidthEdit.Text = strikerHeightEdit.Text;
			}

			if (CheckCellsEmpty())
			{
				return;
			}

			List<double> listHammerParameters = new List<double>();

			listHammerParameters = new List<double>()
			{
				Convert.ToDouble(headLengthEdit.Text),
				Convert.ToDouble(headHeightEdit.Text),
				Convert.ToDouble(headWidthEdit.Text),
				headShapeComboBox.SelectedIndex,
				Convert.ToDouble(strikerHeightEdit.Text),
				Convert.ToDouble(strikerWidthEdit.Text),
				Convert.ToDouble(stickLengthEdit.Text),
				Convert.ToDouble(stickDiameterEdit.Text),
			};

			_hammerParameters = new HammerParameters(listHammerParameters);

			if (_hammerParameters.HeadParam == null ||
				_hammerParameters.StrikerParam == null ||
				_hammerParameters.StickParam == null)
			{
				return;
			}

			_inventorAPI = new InventorAPI();

			_hammerManager = new HammerManager(_inventorAPI,
				_hammerParameters);

			_hammerManager.CreateHammer();
		}


		/// <summary>
		/// Check if the cell is empty
		/// </summary>
		/// <returns> Empty cell </returns>
		private bool CheckCellsEmpty()
		{
			bool cellsNotEmpty = false;

			foreach (var i in _typeErrorToMaskedTextBoxDictionary.Values)
			{
				if (i.Text == string.Empty)
				{
					i.BorderColor = System.Drawing.Color.Red;
					cellsNotEmpty = true;
				}
			}
			
			return cellsNotEmpty;
		}


		/// <summary>
		/// Change the color to the default color when the MaskedTextBoxWithBorderColor
		/// field changes
		/// </summary>
		private void MaskedTextBoxWithBorderColor_TextChanged(object sender, EventArgs e)
		{
			((MaskedTextBoxWithBorderColor)sender).BorderColor = System.Drawing.Color.Gray;
		}


		/// <summary>
		/// Restrict the input field 
		/// </summary>
		private void MaskedTextBoxWithBorderColor_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (((MaskedTextBoxWithBorderColor)sender).Name == "stickLengthEdit")
			{
				ValueParameterChecking.CheckNoMoreNChar(
					(MaskedTextBoxWithBorderColor)sender, e, 4);
			}
			else
			{
				ValueParameterChecking.CheckNoMoreNChar(
					(MaskedTextBoxWithBorderColor)sender, e, 3);
			}

			ValueParameterChecking.CheckOnlyNumbers(e);
		}

		
		/// <summary>
		/// Disable keystrokesin filed headShapeComboBox
		/// </summary>
		private void HeadShapeComboBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.KeyChar = '\0';
		}


		/// <summary>
		/// Changing the enable fields 
		/// headWidthEdit and strikerWidthEdit
		/// </summary>
		private void headShapeComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			headWidthEdit.Enabled = 
				(headShapeComboBox.SelectedIndex == (int)FormHead.Prism);
			strikerWidthEdit.Enabled = 
				(headShapeComboBox.SelectedIndex == (int)FormHead.Prism);
		}
	}
}

