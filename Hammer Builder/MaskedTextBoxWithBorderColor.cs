﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace HammerBuilder
{
	/// <summary>
	/// MaskedTextBox with the ability to change the color of the frame
	/// </summary>
	class MaskedTextBoxWithBorderColor : MaskedTextBox
	{
		/// <summary>
		/// Color border
		/// </summary>
		private Color _color = Color.Black;


		/// <summary>
		/// Changed BorderStyle 
		/// </summary>
		public MaskedTextBoxWithBorderColor()
		{
			this.BorderStyle = BorderStyle.FixedSingle;
		}


		/// <summary>
		/// Color border
		/// </summary>
		[DefaultValue(typeof(Color), "Black")]
		public Color BorderColor
		{
			get { return _color; }
			set { _color = value; Invalidate(); }
		}


		/// <summary>
		/// Changed color border
		/// </summary>
		private void DrawBorder()
		{
			using (Graphics gr = this.CreateGraphics())
			{
				ControlPaint.DrawBorder(gr, this.DisplayRectangle, _color, ButtonBorderStyle.Solid);
			}
		}


		/// <summary>
		/// Message handler windows
		/// </summary>
		/// <param name="m"> System message </param>
		protected override void WndProc(ref Message m)
		{
			const int idOpeation = 15;

			base.WndProc(ref m);
			if (m.Msg == idOpeation) DrawBorder();
		}
	}
}
