﻿using System;
using System.Diagnostics;
using Environment = System.Environment;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using HammerBuilder;
using System.IO;


namespace UnitTests
{
	[TestFixture]
	public class StressComponentBuild
	{
		private readonly StreamWriter _writer;
		private PerformanceCounter _ramCounter;
		private PerformanceCounter _cpuCounter;


		public StressComponentBuild()
		{
			_writer = new StreamWriter(@".\StressTest.txt");
		}


		[Test]
		public void Start()
		{
			int n = 0;
			while (true)
			{
				InventorAPI inventorAPI = new InventorAPI();

				List<double> listHammerParameters = new List<double>()
				{
					100, 50, 50, 0, 25, 25, 300, 10
				};

				HammerParameters hammerParameters = new HammerParameters(listHammerParameters);

				HammerManager hammerManager = new HammerManager(inventorAPI,
					hammerParameters);

				var processes = Process.GetProcessesByName("Inventor");
				var process = processes[4];

				// При первой итерации проинициализировать объекты, отвечающие за фиксирование нагрузки
				if (n == 0)
				{
					_ramCounter = new PerformanceCounter("Process", "Working Set", process.ProcessName);
					_cpuCounter = new PerformanceCounter("Process", "% Processor Time", process.ProcessName);
				}

				_cpuCounter.NextValue();

				hammerManager.CreateHammer();

				var ram = _ramCounter.NextValue();
				var cpu = _cpuCounter.NextValue();

				_writer.Write($"{n}. ");
				_writer.Write($"RAM: {Math.Round(ram / 1024 / 1024)} MB");
				_writer.Write($"\tCPU: {cpu} %");
				_writer.Write(Environment.NewLine);
				_writer.Flush();
				n += 1;
			}
		}

		public void CloseApplication()
		{
			foreach (var process in Process.GetProcessesByName("Inventor"))
			{
				process.Kill();
			}
		}
	}
}
