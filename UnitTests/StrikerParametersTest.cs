﻿using NUnit.Framework;
using HammerBuilder;

namespace UnitTests
{
	[TestFixture]
	class StrikerParametersTest
	{
		[Test, Description("Constructor test with input values")]
		[TestCase(0, 0, Description = "Pass 0 and 0")]
		[TestCase(-1, -1, Description = "Pass -1 and -1")]
		[TestCase(double.NaN, double.NaN,
			Description = "Pass NaN and NaN")]
		[TestCase(double.NegativeInfinity, double.NegativeInfinity,
			Description = "Pass NegativeInfinity and NegativeInfinity")]
		[TestCase(double.PositiveInfinity, double.PositiveInfinity,
			Description = "Pass PositiveInfinity and Positiveinfinity")]
		public void TestConstructor_Negative(double height, double width)
		{
			StrikerParameters strikerParameters = new StrikerParameters(height, width);

			Assert.AreEqual(strikerParameters.Height, height);
			Assert.AreEqual(strikerParameters.Width, width);
		}


		[Test, Description("Constructor test with input values")]
		[TestCase(20, 20, Description = "Pass 20 and 20")]
		[TestCase(25, 25, Description = "Pass 25 and 25")]
		public void TestConstructor_Positive(double height, double width)
		{
			StrikerParameters strikerParameters = new StrikerParameters(height, width);

			Assert.AreEqual(strikerParameters.Height, height);
			Assert.AreEqual(strikerParameters.Width, width);
		}
	}
}
