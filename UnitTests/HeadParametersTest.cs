﻿using NUnit.Framework;
using HammerBuilder;

namespace UnitTests
{
	[TestFixture]
	class HeadParametersTest
	{
		[Test, Description("Constructor test with input values")]
		[TestCase(0, 0, 0, FormHead.Cylinder,
			Description = "Pass 0, 0, 0 and Cylinder")]
		[TestCase(-1, -1, -1, FormHead.Prism,
			Description = "Pass -1, -1, -1 and Prism")]
		[TestCase(double.NaN, double.NaN, double.NaN, FormHead.Prism,
			Description = "Pass NaN, NaN, NaN and Prism")]
		[TestCase(double.NegativeInfinity, double.NegativeInfinity,
			double.NegativeInfinity, FormHead.Prism,
			Description = "Pass NegativeInfinity, NegativeInfinity," +
			" NegativeInfinity and Prism")]
		[TestCase(double.PositiveInfinity, double.PositiveInfinity,
			double.PositiveInfinity, FormHead.Cylinder,
			Description = "Pass PositiveInfinity, PositiveInfinity," +
			" PositiveInfinity and Cylinder")]
		public void TestConstructor_Negative(double length, double height, double width, FormHead form)
		{
			HeadParameters headParameters = new HeadParameters(length, height, width, form);

			Assert.AreEqual(headParameters.Length, length);
			Assert.AreEqual(headParameters.Height, height);
			Assert.AreEqual(headParameters.Width, width);
			Assert.AreEqual(headParameters.Form, form);
		}


		[Test, Description("Constructor test with input values")]
		[TestCase(100, 50, 50, FormHead.Cylinder)]
		[TestCase(110, 45, 45, FormHead.Prism)]
		public void TestConstructor_Positive(double length, double height, double width, FormHead form)
		{
			HeadParameters headParameters = new HeadParameters(length, height, width, form);

			Assert.AreEqual(headParameters.Length, length);
			Assert.AreEqual(headParameters.Height, height);
			Assert.AreEqual(headParameters.Width, width);
			Assert.AreEqual(headParameters.Form, form);
		}
	}
}
