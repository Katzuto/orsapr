﻿using NUnit.Framework;
using HammerBuilder;
using System.Collections.Generic;

namespace UnitTests
{
	[TestFixture]
	class HammerParametersTest
	{
		[Test, Description("Constructor test with input values")]
		public void TestConstructor_Positive()
		{
			List<double> hammerParametersList = new List<double>()
			{
				100,
				50,
				50,
				0,
				25,
				25,
				300,
				10
			};

			HammerParameters hammerParameters = new HammerParameters(hammerParametersList);

			Assert.AreEqual(hammerParameters.HeadParam.Length, 100);
			Assert.AreEqual(hammerParameters.HeadParam.Height, 50);
			Assert.AreEqual(hammerParameters.HeadParam.Width, 50);
			Assert.AreEqual(hammerParameters.HeadParam.Form, FormHead.Prism);
			Assert.AreEqual(hammerParameters.StrikerParam.Height, 25);
			Assert.AreEqual(hammerParameters.StrikerParam.Width, 25);
			Assert.AreEqual(hammerParameters.StickParam.Length, 300);
			Assert.AreEqual(hammerParameters.StickParam.Diameter, 10);
		}


		[Test, Description("Constructor test with input values")]
		public void TestConstructor_Negative()
		{
			List<double> hammerParametersList = new List<double>()
			{
				-1,
				-1,
				-1,
				-1,
				-1,
				-1,
				-1,
				-1
			};

			try
			{
				HammerParameters hammerParameters = new HammerParameters(hammerParametersList);
			}
			catch (System.NullReferenceException exception)
			{
				Assert.AreEqual("Ссылка на объект не указывает на экземпляр объекта.", exception.Message);
			}
		}
	}
}
